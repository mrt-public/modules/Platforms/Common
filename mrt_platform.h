/**
  *@file platform.h
  *@brief Abstraction header
  *@author Jason Berger
  *@date 3/6/2019
  */
#ifndef MRT_PLATFORM_H
#define MRT_PLATFORM_H

#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C"
{
#endif
#include "mrt_platform_common.h"
#include "mrt_FreeRTOS.h"
#include "mrt_zephyr.h"


typedef uint32_t mrt_status_t;

#define MRT_PLATFORM_NONE           1
#define MRT_STM32_HAL               2
#define MRT_ATMEL_START             3
#define MRT_ATMEL_ASF               4
#define MRT_LINUX                   5
#define MRT_ESP32                   6
#define MRT_NRF5                    7
#define MRT_OPENCM3                 8
#define MRT_ATMEL_AVR               9
#define MRT_PLATFORM_CUSTOM         99



#if MRT_PLATFORM == MRT_STM32_HAL
  #include "platforms/stm32/stm32_hal_abstract.h"
  #define MRT_PLATFORM_STRING "STM32_HAL"
	#include "platform_check.h"
#endif

#if MRT_PLATFORM == MRT_ATMEL_START
  #include "platforms/atmel/atmel_start_abstract.h"
  #define MRT_PLATFORM_STRING "Atmel_Start"
#endif

#if MRT_PLATFORM == MRT_ATMEL_ASF
  #include "platforms/atmel/atmel_asf_abstract.h"
  #define MRT_PLATFORM_STRING "Atmel_ASF"
#endif

#if MRT_PLATFORM == MRT_ATMEL_AVR
  #include "platforms/atmel/atmel_avr_abstract.h"
  #define MRT_PLATFORM_STRING "Atmel_AVR"
#endif

#if MRT_PLATFORM == MRT_LINUX
  #include "platforms/linux/linux_abstract.h"
  #define MRT_PLATFORM_STRING "Linux"
#endif

#if MRT_PLATFORM == MRT_ESP32
  #include "platforms/esp32/esp32_abstract.h"
  #define MRT_PLATFORM_STRING "ESP32"
#endif

#if MRT_PLATFORM == MRT_NRF5
  #include "platforms/nrf5/nrf5_abstract.h"
  #define MRT_PLATFORM_STRING "ESP32"
#endif

#if MRT_PLATFORM == MRT_OPENCM3
  #include "platforms/opencm3/opencm3_abstract.h"
  #define MRT_PLATFORM_STRING "OpenCM3"
#endif

#if MRT_PLATFORM == MRT_PLATFORM_CUSTOM
  #define MRT_PLATFORM_STRING "Custom"
  #message "Platform is set to custom, be sure to provide a customized 'mrt_platform_custom.h' file"
  #include "mrt_platform_custom.h"
#endif

#if MRT_PLATFORM == MRT_PLATFORM_NONE
  #define MRT_PLATFORM_STRING "NONE"
  #include "none_abstract.h"
#endif


//platform_check must be included AFTER the abstraction header
#ifndef MRT_PLATFORM_NONE
#include "platform_check.h"
#endif
#ifdef __cplusplus
}
#endif
#endif
