/**
  *@file mrt_zephyr.h
  *@brief Abstraction header zephyr
  *@author Jason Berger
  *@date 2/6/2025
  */


#pragma once

#ifdef MRT_OS_ZEPHYR

#include <zephyr/kernel.h>
#include <stdlib.h>

// #define malloc(size) k_malloc(size)
// #define free(ptr) k_free(ptr)

// #define MRT_MUTEX_TYPE struct k_mutex
// #define MRT_MUTEX_CREATE(m) k_mutex_init(&m)
// #define MRT_MUTEX_LOCK(m) k_mutex_lock(&m, K_FOREVER)
// #define MRT_MUTEX_UNLOCK(m) k_mutex_unlock(&m)
// #define MRT_MUTEX_DELETE(m) k_mutex_unlock(&m)

#define MRT_MUTEX_TYPE uint8_t
#define MRT_MUTEX_CREATE(m) (m) = 0
#define MRT_MUTEX_LOCK(m) (m) = 1
#define MRT_MUTEX_UNLOCK(m) (m) = 0
#define MRT_MUTEX_DELETE(m) (m) =0


#endif //MRT_OS_ZEPHYR